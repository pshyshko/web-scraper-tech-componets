import asyncio
import re
import logging
from bs4 import BeautifulSoup
from scraper import WebScraperAsync


class WebScraperXKOM(WebScraperAsync):
    @classmethod
    def _get_max_page(cls, body: str) -> int:
        try:
            soup = BeautifulSoup(body, "html.parser")
            amount_of_pages = soup.find(
                "span", {"class": "sc-11oikyw-2 ekasrY"})
            amount_of_pages = int(amount_of_pages.string[2:])
            return amount_of_pages
        except Exception:
            logging.info('Error with extract max number pages')

    @classmethod
    def _generate_list_urls(cls, url: str, max_page: int) -> list[str]:
        urls = [url[:-1] + f"{i}" for i in range(1, max_page + 1)]
        return urls

    @classmethod
    def _extract_links_from_page(cls, content: str) -> list[str]:
        soup = BeautifulSoup(content, "html.parser")
        container = soup.find(id="listing-container")
        items = container.find_all(
            "div", {"class": "sc-1yu46qn-6 kUnYNG sc-2ride2-0 eYsBmG"}
        )
        urls = []
        for item in items:
            url = "https://www.x-kom.pl/" + item.a["href"]
            urls.append(url)

        return urls

    @classmethod
    def _extract_from_form(cls, content: str) -> list:
        try:
            soup = BeautifulSoup(content, "html.parser")
        except Exception:
            print("error at _extract_from_form")

        try:
            name = soup.find("h1", {"class": re.compile("sc-1bker4h-4*")})
            name = name.get_text()
        except Exception:
            name = None

        try:
            price = soup.find("div", {"class": "u7xnnm-4 jFbqvs"})
            price = price.string.replace(",", ".")
            price = price.replace(" ", "")
            price = float(price[:-3])
        except Exception:
            price = None

        try:
            available = soup.find(
                "span", {"class": re.compile("sc-1jultii-1 (fmqOOM|fmqOOM)")}
            )
            available = available.get_text()
            available = False if available == "Czasowo niedostępny" else True
        except Exception:
            available = None

        try:
            content = soup.find("link", {"rel": "canonical"})
            url = content["href"]
        except Exception:
            url = None

        result = [name, price, available, url]
        logging.info(result)
        return result

    def _processing_component(self, url: str) -> list[list]:
        content = asyncio.run(self._get_page(url))
        max_page = self._get_max_page(content)
        urls = self._generate_list_urls(url, max_page)

        contents = asyncio.run(self._get_multiply_pages(urls))

        link_to_products = []
        for content in contents:
            body = self._extract_links_from_page(content)
            link_to_products.extend(body)

        values = []
        contents = asyncio.run(self._get_multiply_pages(link_to_products))
        for content in contents:
            info_item_by = self._extract_from_form(content)
            values.append(info_item_by)

        return values

    def run(self):
        for title, url_component in self.url_components.items():
            data = self._processing_component(url_component)
            self.save_result(data, title)


if __name__ == "__main__":

    urls_x = {
        "mother_board":
        "https://www.x-kom.pl/g-5/c/14-plyty-glowne.html?page=1",
        "graphic_card":
        "https://www.x-kom.pl/g-5/c/345-karty-graficzne.html?page=1",
        "proc":
        "https://www.x-kom.pl/g-5/c/11-procesory.html?page=1",
    }

    a = WebScraperXKOM(urls_x, "x-kom")
    a.run()
