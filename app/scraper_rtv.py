import asyncio
import logging
from bs4 import BeautifulSoup
from scraper import WebScraperAsync


class WebScraperRTV(WebScraperAsync):
    @classmethod
    def _get_max_page(cls, content: str) -> int:
        try:
            soup = BeautifulSoup(content, "html.parser")
            result = soup.find_all("a", {"class": "paging-number"})
            if len(result) > 1:
                max_page = int(result[-1].text.strip())
                return max_page
            else:
                return 1
        except Exception:
            logging.info('Error with extract max number pages')

    @classmethod
    def _generate_list_urls(cls, url: str, max_page: int) -> list[str]:
        urls = [url.replace("strona-1", f"strona-{i}")
                for i in range(1, max_page + 1)]
        return urls

    @classmethod
    def _extract_item_from_form(cls, item: BeautifulSoup):
        try:
            price_content = item.find("div", {"class": "product-prices-box"})
            price_content = price_content.div.div.text.strip()[:-2]
            price = float("".join((i for i in price_content if i.isdigit())))
        except Exception:
            price = None

        try:
            name_content = item.find("h2", {"class": "product-name"})
            name = name_content.a.text.strip()
        except Exception:
            name = None

        try:
            url_content = item.find("h2", {"class": "product-name"})
            url = url_content.a["href"]
            url = "https://www.euro.com.pl/" + url
        except Exception:
            url = None

        available_content = item.find("div", {"class": "action-primary"})
        try:
            available_content = available_content.button.text.strip()
            available = True if available_content == "Do koszyka" else False
        except Exception:
            available = False

        result = [name, price, available, url]
        logging.info(result)
        return result

    @classmethod
    def _extract_items_from_container(cls, content: str):
        soup = BeautifulSoup(content, "html.parser")
        container = soup.find("div", {"id": "products"})
        items = container.find_all("div", {"class": "product-for-list"})
        return items

    def _processing_component(self, url):
        print(url)
        content = asyncio.run(self._get_page(url))
        max_page = self._get_max_page(content)
        urls = self._generate_list_urls(url, max_page)
        contents = asyncio.run(self._get_multiply_pages(urls))

        values = []
        for body in contents:
            items = self._extract_items_from_container(body)
            for item in items:
                value_item = self._extract_item_from_form(item)

                if value_item is not None:
                    values.append(value_item)

        return values

    def run(self):
        for title, url_comp in self.url_components.items():
            data = self._processing_component(url_comp)
            self.save_result(data, title)


if __name__ == "__main__":
    urls_r = {
        "mother_board":
        "https://www.euro.com.pl/plyty-glowne,strona-1.bhtml",
        "graphic_card":
        "https://www.euro.com.pl/karty-graficzne,strona-1.bhtml",
        "proc":
        "https://www.euro.com.pl/procesory,strona-1.bhtml",
    }

    scraper = WebScraperRTV(urls_r, "rtv")
    scraper.run()
