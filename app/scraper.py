from datetime import datetime
import asyncio
import requests
import concurrent.futures as cf
import httpx
import pandas as pd
import os


class WebScraper:
    def __init__(self, url_components: list[str], shop: str):
        self.url_components = url_components
        self.shop = shop

    def _get_page(self, url):
        headers = {"user-agent": "po co zablok1"}
        result = requests.get(url, headers=headers)
        return result

    def _get_multi_pages(self, urls):
        start = datetime.now()
        result = []
        with cf.ThreadPoolExecutor() as executor:
            with requests.Session() as session:
                future = executor.map(self._get_page,
                                      [session] * len(urls),
                                      urls)

            for fut in future:
                result.append(fut)
            print("Completed", datetime.now() - start)

        return result


class WebScraperAsync:
    def __init__(self, url_components: list[str], shop: str):
        self.url_components = url_components
        self.shop = shop

    async def _get_page(self, url: str):
        try:
            async with httpx.AsyncClient() as client:
                respond = await client.get(url, timeout=10)
                return respond.text
        except Exception:
            print("error at request")

    async def _get_multiply_pages(self, urls: tuple[str]):
        tasks = [self._get_page(url) for url in urls]

        responds = []
        for future in asyncio.as_completed(tasks):
            result = await future
            responds.append(result)

        return responds

    def make_folder(self) -> bool:
        os.makedirs(f'data\\{self.shop}')

    def save_result(self, data: list[list], title: str):
        folder_exists = os.path.exists(f'data\\{self.shop}')
        if not folder_exists:
            self.make_folder()

        columns = ["name", "price", "available", "url"]
        dataframe = pd.DataFrame(data, columns=columns)
        dataframe.to_excel(f"data\\{self.shop}\\{self.shop}-{title}.xlsx")
