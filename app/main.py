from scraper_rtv import WebScraperRTV
from scraper_xkom import WebScraperXKOM
import logging
from datetime import datetime
from config import ConfigUrls


logging.basicConfig(
    format="%(asctime)s %(message)s",
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M:%S"
)


def run():
    logging.info('Start')
    time_start = datetime.now()

    scraper_rtv = WebScraperRTV(ConfigUrls.URLS_RTV, 'rtv')
    scraper_xkom = WebScraperXKOM(ConfigUrls.URLS_XKOM, 'xkom')

    scraper_shop = [
        scraper_rtv,
        scraper_xkom
    ]

    for scraper in scraper_shop:
        scraper.run()

    time_end = datetime.now() - time_start
    logging.info(f'End: Time completed {time_end}')


if __name__ == "__main__":
    run()
