class ConfigUrls:
    URLS_RTV = {
        "mother_board":
        "https://www.euro.com.pl/plyty-glowne,strona-1.bhtml",
        "graphic_card":
        "https://www.euro.com.pl/karty-graficzne,strona-1.bhtml",
        "proc":
        "https://www.euro.com.pl/procesory,strona-1.bhtml",
    }

    URLS_XKOM = {
        "mother_board":
        "https://www.x-kom.pl/g-5/c/14-plyty-glowne.html?page=1",
        "graphic_card":
        "https://www.x-kom.pl/g-5/c/345-karty-graficzne.html?page=1",
        "proc":
        "https://www.x-kom.pl/g-5/c/11-procesory.html?page=1",
    }
