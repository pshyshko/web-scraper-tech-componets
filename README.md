## About project
Web scraper to collect information from commercial sites. In particular, it collects information about the price and availability of the item in the store. Supports two approaches for querying the site asynchronous and multithreaded.
	
## Setting up

If necessary, you can add new links to gather information in config.py

## Technologies

Project is created with:
* Python 3.9
	
## Setup
To run this project, install it on local machine using pip.

(Optional)
```
$ cd path/to/project
$ pip install virtualenv 
$ virtualenv name_env
$ .\name_env\Scripts\activate
```
Install:
```
$ pip install -r requirements.txt 
$ python .\app\main.py
```